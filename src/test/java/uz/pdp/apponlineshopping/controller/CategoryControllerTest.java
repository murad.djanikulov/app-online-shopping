package uz.pdp.apponlineshopping.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import uz.pdp.apponlineshopping.payload.AddressReqDto;
import uz.pdp.apponlineshopping.payload.CategoryReqDto;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RequiredArgsConstructor
class CategoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    private CategoryReqDto categoryReqDto = new CategoryReqDto();

    @SneakyThrows
    @Test
    @WithUserDetails("+998123456789")
    void getAll() {
        mockMvc.perform(get("/api/category/get-all")
//        .param()
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.content").exists());

    }

    @Test
    @SneakyThrows
    @WithUserDetails("+998123456789")
    void getCategory(){
        mockMvc.perform(
                get("/api/category/get/{id}", UUID.fromString("a332a5f2-7720-4a71-b272-2dea93480ce2")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").value("a332a5f2-7720-4a71-b272-2dea93480ce2"));
    }

    @Test
    @WithUserDetails("+998123456789")
    @SneakyThrows
    void addCategory(){

        categoryReqDto.setName("Refrigerator");

        String category = objectMapper.writeValueAsString(categoryReqDto);

        mockMvc.perform(
                post("/api/category/add/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(category))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").exists());

    }

    @Test
    @WithUserDetails("+998123456789")
    @SneakyThrows
    void editCategory(){

        categoryReqDto.setName("Freezer");

        String address = objectMapper.writeValueAsString(categoryReqDto);

        mockMvc.perform(
                put("/api/category/edit/{id}", "cf96b2eb-9b77-4551-99d1-b77fedcb4b49")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(address))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").exists());

    }

    @Test
    @WithUserDetails("+998123456789")
    @SneakyThrows
    void deleteCategory(){
        mockMvc.perform(
                delete("/api/category/delete/{id}", "cf96b2eb-9b77-4551-99d1-b77fedcb4b49"))
                .andDo(print())
                .andExpect(status().isOk());

    }
}