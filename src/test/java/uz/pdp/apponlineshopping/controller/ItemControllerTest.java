package uz.pdp.apponlineshopping.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import uz.pdp.apponlineshopping.payload.CategoryReqDto;
import uz.pdp.apponlineshopping.payload.ItemReqDto;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RequiredArgsConstructor
class ItemControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    private ItemReqDto itemReqDto = new ItemReqDto();

    @SneakyThrows
    @Test
    @WithUserDetails("+998123456789")
    void getAllByOrderId() {
        mockMvc.perform(get("/api/item/get-allByOrderId/{orderId}","b826aad3-d8ca-4b4f-befc-495c2ada3a21")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data").exists());

    }

    @Test
    @SneakyThrows
    @WithUserDetails("+998123456789")
    void getItem(){
        mockMvc.perform(
                get("/api/item/get/{id}", UUID.fromString("8a1db584-3b91-443d-a386-a5ee244390eb")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").value("8a1db584-3b91-443d-a386-a5ee244390eb"));
    }

    @Test
    @WithUserDetails("+998123456789")
    @SneakyThrows
    void addItem(){


        itemReqDto.setOrderId(UUID.fromString("b826aad3-d8ca-4b4f-befc-495c2ada3a21"));
        itemReqDto.setProductName("LG");
        itemReqDto.setQuantity(3);

        String item = objectMapper.writeValueAsString(itemReqDto);

        mockMvc.perform(
                post("/api/item/add/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(item))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").exists());
    }

    @Test
    @WithUserDetails("+998123456789")
    @SneakyThrows
    void editItem(){

        itemReqDto.setOrderId(UUID.fromString("b826aad3-d8ca-4b4f-befc-495c2ada3a21"));
        itemReqDto.setProductName("LG");
        itemReqDto.setQuantity(5);

        String item = objectMapper.writeValueAsString(itemReqDto);

        mockMvc.perform(
                put("/api/item/edit/{id}","f0f5bb57-3515-45f3-9199-7b7337953214")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(item))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").exists());
    }


    @Test
    @WithUserDetails("+998123456789")
    @SneakyThrows
    void deleteItem(){
        mockMvc.perform(
                delete("/api/item/delete/{id}", "f0f5bb57-3515-45f3-9199-7b7337953214"))
                .andDo(print())
                .andExpect(status().isOk());

    }
}