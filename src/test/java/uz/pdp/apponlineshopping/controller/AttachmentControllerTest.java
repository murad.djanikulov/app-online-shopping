package uz.pdp.apponlineshopping.controller;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RequiredArgsConstructor
class AttachmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @SneakyThrows
    @WithUserDetails("+998123456789")
    void upload() {

        Resource fileResource = new ClassPathResource("images/microservices.png");

        MockMultipartFile mockMultipartFile=new MockMultipartFile("file",fileResource.getFilename(), MediaType.ALL_VALUE,fileResource.getInputStream());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/attachment/upload")
                .file(mockMultipartFile))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data").exists());

    }

    @Test
    @SneakyThrows
    @WithUserDetails("+998123456789")
    void getAll() {
        mockMvc.perform(get("/api/attachment/info"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.content").exists());

    }

    @Test
    @SneakyThrows
    @WithUserDetails("+998123456789")
    void getById() {
        mockMvc.perform(get("/api/attachment/info/{id}","4aea1435-5f8d-4aa6-95aa-442e202b97e4"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").value("4aea1435-5f8d-4aa6-95aa-442e202b97e4"));

    }

    @Test
    @SneakyThrows
    @WithUserDetails("+998123456789")
    void download() {
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/api/attachment/download/{id}", "34631fd0-2fcd-4eb4-8001-7fb3e808938f"))
//                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        Assertions.assertEquals(mvcResult.getResponse().getContentAsByteArray().length,131368);
        Assertions.assertEquals(mvcResult.getResponse().getContentType(),MediaType.IMAGE_JPEG.toString());

    }}