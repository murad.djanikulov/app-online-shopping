package uz.pdp.apponlineshopping.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import uz.pdp.apponlineshopping.payload.CategoryReqDto;
import uz.pdp.apponlineshopping.payload.ProductReqDto;

import java.util.HashSet;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RequiredArgsConstructor
class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    private ProductReqDto productReqDto = new ProductReqDto();

    @WithUserDetails("+998123456789")
    @SneakyThrows
    @Test
    void getAllByNameSorted() {
        mockMvc.perform(
                get("/api/product/get-all-by-name-sorted"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.content.[0].name").value("Acer"))
                .andExpect(jsonPath("$.data.content.[1].name").value("Apple"))
                .andExpect(jsonPath("$.data.content.[2].name").value("Artel"));
    }

    @WithUserDetails("+998123456789")
    @SneakyThrows
    @Test
    void getAllByPriceSorted() {
        mockMvc.perform(
                get("/api/product/get-all-by-price-sorted"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.content.[0].price").value(300.00))
                .andExpect(jsonPath("$.data.content.[1].price").value(400.00))
                .andExpect(jsonPath("$.data.content.[2].price").value(500.00));

    }

    @WithUserDetails("+998123456789")
    @SneakyThrows
    @Test
    void getAllByCategoryFiltered() {

        mockMvc.perform(
                get("/api/product/get-all-by-category-filtered").param("categoryName","Washing machine"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.[0].categoryId").value("4246da67-052d-46ed-bde8-1e75d0d69100"))
                .andExpect(jsonPath("$.data.[1].categoryId").value("4246da67-052d-46ed-bde8-1e75d0d69100"))
                .andExpect(jsonPath("$.data.[2].categoryId").value("4246da67-052d-46ed-bde8-1e75d0d69100"));

    }

    @WithUserDetails("+998123456789")
    @SneakyThrows
    @Test
    void getAllBySearchName() {

        mockMvc.perform(
                get("/api/product/get-all-by-search-name").param("searchName","A"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.[0].name").value("Apple"))
                .andExpect(jsonPath("$.data.[1].name").value("Artel"))
                .andExpect(jsonPath("$.data.[2].name").value("Acer"));
    }

    @WithUserDetails("+998123456789")
    @SneakyThrows
    @Test
    void getProduct() {
        mockMvc.perform(
                get("/api/product/get/{id}", UUID.fromString("36be7eda-ffe7-47db-8079-38470c54af42")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").value("36be7eda-ffe7-47db-8079-38470c54af42"));
    }

    @WithUserDetails("+998123456789")
    @SneakyThrows
    @Test
    void addProduct() {

        productReqDto.setName("Indesit");
        productReqDto.setCategoryName("Washing machine");
        productReqDto.setDescription("Brand new ");
        productReqDto.setPrice(450.00);
        productReqDto.setAttachmentIds(new HashSet<>());

        String product = objectMapper.writeValueAsString(productReqDto);

        mockMvc.perform(
                post("/api/product/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(product))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").exists());

    }

    @WithUserDetails("+998123456789")
    @SneakyThrows
    @Test
    void editProduct() {

        productReqDto.setName("Midea");
        productReqDto.setCategoryName("Washing machine");
        productReqDto.setDescription("Brand new ");
        productReqDto.setPrice(650.00);
        productReqDto.setAttachmentIds(new HashSet<>());

        String product = objectMapper.writeValueAsString(productReqDto);

        mockMvc.perform(
                put("/api/product/edit/{id}", "36be7eda-ffe7-47db-8079-38470c54af42")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(product))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").exists());

    }

    @Test
    @WithUserDetails("+998123456789")
    @SneakyThrows
    void deleteProduct(){
        mockMvc.perform(
                delete("/api/product/delete/{id}", "36be7eda-ffe7-47db-8079-38470c54af42"))
                .andDo(print())
                .andExpect(status().isOk());

    }
}