package uz.pdp.apponlineshopping.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import uz.pdp.apponlineshopping.payload.LoginDto;
import uz.pdp.apponlineshopping.payload.UserDto;
import uz.pdp.apponlineshopping.payload.TokenDto;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RequiredArgsConstructor
class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    private LoginDto loginDto = new LoginDto();

    private UserDto userDto = new UserDto();

    private TokenDto tokenDto=new TokenDto();



    @SneakyThrows
    @Test
    void signIn() {

        loginDto.setPhoneNumber("+998123456789");
        loginDto.setPassword("0123456789");
        String login = objectMapper.writeValueAsString(loginDto);

        mockMvc.perform(
                post("/api/auth/sign-in")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(login))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.accessToken").exists()).andExpect(jsonPath("$.data.refreshToken").exists());

    }

    @SneakyThrows
    @Test
    void signUp() {
        userDto.setFirstName("user");
        userDto.setLastName("user");
        userDto.setPhoneNumber("+998987654321");
        userDto.setPassword("0123456789");
        String user = objectMapper.writeValueAsString(userDto);

        mockMvc.perform(
                post("/api/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(user))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.accessToken").exists()).andExpect(jsonPath("$.data.refreshToken").exists());

    }

    @SneakyThrows
    @Test
    void refreshToken() {

        tokenDto.setAccessToken(
                "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJlODg3MzdkOS01NmMzLTRhYmItOGZkYS1hMDgyNGJiMjJiZDUiLCJpYXQiOjE2NTYzMTQ2MTUsImV4cCI6MTY1NjQwMTAxNX0.JxeScDEq-I_ZtEuiAOgbLXMGa5D2LQ9k31sITAvWCGZ-pDAfHQIJqxR1GwnxjThbpSaMhdoGa3es4_JmnYKbjQ"
        );
        tokenDto.setRefreshToken(
                "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJlODg3MzdkOS01NmMzLTRhYmItOGZkYS1hMDgyNGJiMjJiZDUiLCJpYXQiOjE2NTYzMTQ2MTUsImV4cCI6MTY1NjQ4NzQxNX0.sKMoXe-y-MCY4hSqQM7UUzwBT2yps8kKpGZPzK3SxLcEy1CeJw-GDd9OcUCIv0GjNFo0cShR7ZjW_g756zUE6w"
        );

        String token = objectMapper.writeValueAsString(tokenDto);

        mockMvc.perform(
                post("/api/auth/refresh-token")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(token))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.accessToken").exists()).andExpect(jsonPath("$.data.refreshToken").exists());

    }

    @SneakyThrows
    @Test
    @WithUserDetails("+998987654321")
    void editUser() {
        userDto.setFirstName("ADMIN");
        userDto.setLastName("ADMIN");
        userDto.setPhoneNumber("+998123456789");
        userDto.setPassword("0123456789");
        String user = objectMapper.writeValueAsString(userDto);

        mockMvc.perform(
                post("/api/auth/edit-user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(user))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @SneakyThrows
    @Test
    @WithUserDetails("+998123456789")
    void deleteUser() {

        mockMvc.perform(
                post("/api/auth/delete-user"))
                .andDo(print())
                .andExpect(status().isOk());
    }

}