package uz.pdp.apponlineshopping.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RequiredArgsConstructor
class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    @SneakyThrows
    @WithUserDetails("+998123456789")
    void confirmAndPay() {
        mockMvc.perform(
                put("/api/order/confirmAndPay/{id}", "b826aad3-d8ca-4b4f-befc-495c2ada3a21"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").exists());
    }

    @Test
    @SneakyThrows
    @WithUserDetails("+998123456789")
    void getOrder() {
        mockMvc.perform(
                get("/api/order/get/{id}", "b826aad3-d8ca-4b4f-befc-495c2ada3a21"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").exists());
    }

    @Test
    @SneakyThrows
    @WithUserDetails("+998123456789")
    void addOrder() {
        mockMvc.perform(
                post("/api/order/add"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").exists());
    }
    @Test
    @SneakyThrows
    @WithUserDetails("+998123456789")
    void deleteOrder() {
        mockMvc.perform(
                delete("/api/order/delete/{orderId}", "94993360-5cc7-48d9-b875-fba48b4bc3b9"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}