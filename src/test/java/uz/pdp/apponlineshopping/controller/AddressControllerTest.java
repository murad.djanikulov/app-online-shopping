package uz.pdp.apponlineshopping.controller;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import uz.pdp.apponlineshopping.payload.AddressReqDto;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@RequiredArgsConstructor
class AddressControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    private AddressReqDto addressReqDto = new AddressReqDto();


    @Test
    @SneakyThrows
    @WithUserDetails("+998123456789")
    void getAll(){
        mockMvc.perform(get("/api/address/get-all")
//        .param()
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.content").exists());

    }

    @Test
    @SneakyThrows
    @WithUserDetails("+998123456789")
    void getAddress(){
        mockMvc.perform(
                get("/api/address/get/{id}", UUID.fromString("467ea2a8-bfad-4621-a126-6eef461b66f6")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").value("467ea2a8-bfad-4621-a126-6eef461b66f6"));
    }

    @Test
    @WithUserDetails("+998123456789")
    @SneakyThrows
    void addAddress() {
        addressReqDto.setState("Tashkent");
        addressReqDto.setCity("Yangiyul");
        addressReqDto.setStreet("Registon");
        addressReqDto.setPostalCode("123456789");
        addressReqDto.setHomeNumber("123");

        String address = objectMapper.writeValueAsString(addressReqDto);

        mockMvc.perform(
                post("/api/address/add/{orderId}", "b826aad3-d8ca-4b4f-befc-495c2ada3a21")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(address))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").exists());

    }

    @Test
    @WithUserDetails("+998123456789")
    @SneakyThrows
    void editAddress(){
        addressReqDto.setState("Samarkand");
        addressReqDto.setCity("Urgut");
        addressReqDto.setStreet("Nihol");
        addressReqDto.setPostalCode("987654321");
        addressReqDto.setHomeNumber("321");

        String address = objectMapper.writeValueAsString(addressReqDto);

        mockMvc.perform(
                put("/api/address/edit/{orderId}", "fb83190c-5e92-4ebf-9549-99802dac1bb2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(address))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").exists());

    }

    @Test
    @WithUserDetails("+998123456789")
    @SneakyThrows
    void deleteAddress(){
        mockMvc.perform(
                delete("/api/address/delete/{orderId}", "fb83190c-5e92-4ebf-9549-99802dac1bb2"))
                .andDo(print())
                .andExpect(status().isOk());

    }
}