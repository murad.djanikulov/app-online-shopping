package uz.pdp.apponlineshopping.utils;

public interface RestConstant {

    String BASE_PATH = "/api";

    String DOMAIN = "http://localhost";

    String AUTH_CONTROLLER = BASE_PATH + "/auth";
    String ATTACHMENT_CONTROLLER = BASE_PATH + "/attachment";
    String ADDRESS_CONTROLLER = BASE_PATH + "/address";
    String ORDER_CONTROLLER = BASE_PATH + "/order";
    String ITEM_CONTROLLER = BASE_PATH + "/item";
    String PRODUCT_CONTROLLER = BASE_PATH + "/product";
    String CATEGORY_CONTROLLER = BASE_PATH + "/category";
    String WEATHER_CONTROLLER = BASE_PATH + "/weather";
    String LOCALE_CHANGE_CONTROLLER = BASE_PATH + "/language";

}
