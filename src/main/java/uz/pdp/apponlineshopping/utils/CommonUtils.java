package uz.pdp.apponlineshopping.utils;

import java.util.UUID;

public class CommonUtils {
    public static String buildPhotoUrl(UUID id) {
        return RestConstant.DOMAIN + RestConstant.ATTACHMENT_CONTROLLER + "/download/" + id;
    }
}
