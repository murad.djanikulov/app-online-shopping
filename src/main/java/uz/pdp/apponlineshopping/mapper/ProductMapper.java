package uz.pdp.apponlineshopping.mapper;


import org.mapstruct.Mapper;
import uz.pdp.apponlineshopping.entity.Product;
import uz.pdp.apponlineshopping.entity.template.AbsEntity;
import uz.pdp.apponlineshopping.payload.ProductResDto;

import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    default ProductResDto productToDto(Product product){
        return new ProductResDto(
                product.getId(),
                product.getName(),
                product.getPrice(),
                product.getDescription(),
                product.getCategory().getId(),
                product.getAttachments().stream().map(AbsEntity::getId).collect(Collectors.toSet())
        );
    }
}
