package uz.pdp.apponlineshopping.mapper;


import org.mapstruct.Mapper;
import uz.pdp.apponlineshopping.entity.Category;
import uz.pdp.apponlineshopping.payload.CategoryResDto;

@Mapper(componentModel = "spring")
public interface CategoryMapper {
    CategoryResDto categoryToDto(Category category);
}
