package uz.pdp.apponlineshopping.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import uz.pdp.apponlineshopping.entity.Address;
import uz.pdp.apponlineshopping.payload.AddressResDto;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    //    @Mapping(source = "", target = "")
    default AddressResDto addressToDto(Address address) {
        return new AddressResDto(
                address.getId(),
                address.getState(),
                address.getCity(),
                address.getStreet(),
                address.getPostalCode(),
                address.getHomeNumber(),
                address.getOrder().getId()
        );

    }

    ;
}
