package uz.pdp.apponlineshopping.service;

import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.apponlineshopping.component.MessageByLang;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.exception.RestException;
import uz.pdp.apponlineshopping.payload.LoginDto;
import uz.pdp.apponlineshopping.payload.UserDto;
import uz.pdp.apponlineshopping.payload.TokenDto;
import uz.pdp.apponlineshopping.repository.RoleRepository;
import uz.pdp.apponlineshopping.repository.UserRepository;
import uz.pdp.apponlineshopping.security.JWTProvider;
import uz.pdp.apponlineshopping.utils.AppConstant;

import java.util.Optional;
import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {
    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JWTProvider jwtProvider;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final MessageByLang messageByLang;

    public AuthServiceImpl(UserRepository userRepository, @Lazy AuthenticationManager authenticationManager, @Lazy JWTProvider jwtProvider, RoleRepository roleRepository, PasswordEncoder passwordEncoder, MessageByLang messageByLang) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.messageByLang = messageByLang;
    }

    @Override
    public ApiResult<TokenDto> signIn(LoginDto loginDto) {
        //        matching passwordEncoder and commanding loadUserByUsername
        try {
            Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    loginDto.getPhoneNumber(),
                    loginDto.getPassword()
            ));
            User user = (User) authenticate.getPrincipal();
            String refreshToken = jwtProvider.generateTokenFromId(user.getId(), false);
            String accessToken = jwtProvider.generateTokenFromId(user.getId(), true);
            return ApiResult.successResponse(new TokenDto(accessToken, refreshToken));
        } catch (Exception e) {
            throw new RestException(HttpStatus.UNAUTHORIZED, messageByLang.getMessageByKey("WRONG_PASSWORD_OR_USERNAME"));
        }
    }


    @Override
    public ApiResult<TokenDto> signUp(UserDto userDto) {
        Optional<User> existedPhoneNumber = userRepository.findByPhoneNumber(userDto.getPhoneNumber());
        if (existedPhoneNumber.isPresent())
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("EXISTED_PHONE_NUMBER"));

        User user = new User(
                userDto.getFirstName(),
                userDto.getLastName(),
                userDto.getPhoneNumber(),
                passwordEncoder.encode(userDto.getPassword()),
                roleRepository.findByName(AppConstant.USER).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ROLE_NOT_FOUND")))
        );
        userRepository.save(user);
        String accessToken = jwtProvider.generateTokenFromId(user.getId(), true);
        String refreshToken = jwtProvider.generateTokenFromId(user.getId(), false);
        return ApiResult.successResponse(new TokenDto(accessToken, refreshToken));
    }

    @Override
    public ApiResult<TokenDto> refreshToken(TokenDto tokenDto) {
        try {
            jwtProvider.validateToken(tokenDto.getAccessToken());
            return ApiResult.successResponse(tokenDto);
        } catch (ExpiredJwtException e) {
            try {
                jwtProvider.validateToken(tokenDto.getRefreshToken());
                UUID userId = UUID.fromString(jwtProvider.getIdFromToken(tokenDto.getRefreshToken()));
                return ApiResult.successResponse(new TokenDto(
                        jwtProvider.generateTokenFromId(userId, true),
                        jwtProvider.generateTokenFromId(userId, false)
                ));
            } catch (Exception ex) {
                throw new RestException(HttpStatus.UNAUTHORIZED, messageByLang.getMessageByKey("CORRUPTED_REFRESH"));
            }
        } catch (Exception e) {
            throw new RestException(HttpStatus.UNAUTHORIZED, messageByLang.getMessageByKey("CORRUPTED_ACCESS"));
        }
    }

    @Override
    public ApiResult<?> editUser(UserDto userDto, User user) {
        Optional<User> existedPhoneNumber = userRepository.findByPhoneNumberAndIdNot(userDto.getPhoneNumber(), user.getId());
        if (existedPhoneNumber.isPresent())
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("EXISTED_PHONE_NUMBER"));

        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userRepository.save(user);

        return ApiResult.successResponse(messageByLang.getMessageByKey("USER_EDITED"));
    }

    @Override
    public ApiResult<?> deleteUser(User user) {
        try {
            userRepository.deleteById(user.getId());
            return ApiResult.successResponse(messageByLang.getMessageByKey("USER_DELETED"));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("USER_NOT_FOUND"));
        }
    }

    //    UserDetails Method
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByPhoneNumber(s).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("USER_NOT_FOUND")));
    }

    //    Custom method
    @Override
    public UserDetails loadById(UUID id) {
        return userRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("USER_NOT_FOUND")));
    }
}
