package uz.pdp.apponlineshopping.service;

import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.payload.OrderResDto;

import java.util.UUID;

public interface OrderService {

    ApiResult<OrderResDto> confirmAndPay(UUID id,User user);

    ApiResult<OrderResDto> getOrder(UUID id,User user);

    ApiResult<OrderResDto> addOrder(User user);

    ApiResult<?> deleteOrder(UUID id,User user);

}
