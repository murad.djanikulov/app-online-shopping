package uz.pdp.apponlineshopping.service;

import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.ItemReqDto;
import uz.pdp.apponlineshopping.payload.ItemResDto;

import java.util.List;
import java.util.UUID;

public interface ItemService {

    ApiResult<List<ItemResDto>> getAllByOrderId(UUID orderId, User user);

    ApiResult<ItemResDto> getItem(UUID id,User user);

    ApiResult<ItemResDto> addItem(ItemReqDto itemReqDto, User user);

    ApiResult<ItemResDto> editItem(ItemReqDto itemReqDto, UUID id, User user);

    ApiResult<?> deleteItem(UUID id, User user);

}
