package uz.pdp.apponlineshopping.service;

import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.CategoryReqDto;
import uz.pdp.apponlineshopping.payload.CategoryResDto;

import java.util.UUID;

public interface CategoryService{
    ApiResult<CustomPage<CategoryResDto>> getAll(int page, int size);

    ApiResult<CategoryResDto> getCategory(UUID id);

    ApiResult<CategoryResDto> addCategory(CategoryReqDto categoryReqDto);

    ApiResult<CategoryResDto> editCategory(CategoryReqDto categoryReqDto, UUID id);

    ApiResult<?> deleteCategory(UUID id);

}
