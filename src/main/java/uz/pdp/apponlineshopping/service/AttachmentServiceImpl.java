package uz.pdp.apponlineshopping.service;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.apponlineshopping.component.MessageByLang;
import uz.pdp.apponlineshopping.entity.Attachment;
import uz.pdp.apponlineshopping.entity.AttachmentContent;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.exception.RestException;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.repository.AttachmentContentRepository;
import uz.pdp.apponlineshopping.repository.AttachmentRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AttachmentServiceImpl implements AttachmentService {

    private final AttachmentRepository attachmentRepository;
    private final AttachmentContentRepository attachmentContentRepository;
    private final MessageByLang messageByLang;

    @Override
    public ApiResult<?> upload(MultipartFile file) throws IOException {
        if (file != null) {
            String originalFilename = file.getOriginalFilename();
            Attachment attachment = new Attachment(
                    originalFilename,
                    file.getContentType(),
                    file.getSize(),null
            );
            Attachment save = attachmentRepository.save(attachment);
            AttachmentContent attachmentContent = new AttachmentContent(
                    file.getBytes(),
                    save
            );
            attachmentContentRepository.save(attachmentContent);
            return ApiResult.successResponse(attachment.getId());
        }
        throw new RestException(HttpStatus.BAD_REQUEST,messageByLang.getMessageByKey("ERROR_IN_UPLOADING_FILE"));
    }

    @Override
    public ApiResult<CustomPage<Attachment>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Attachment> attachmentPage = attachmentRepository.findAll(pageable);
        CustomPage<Attachment> attachmentCustomPage = attachmentCustomPage(attachmentPage);
        return ApiResult.successResponse(attachmentCustomPage);
    }

    @Override
    public ApiResult<Attachment> getById(UUID id) {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND,messageByLang.getMessageByKey("FILE_NOT_FOUND")));
        return ApiResult.successResponse(attachment);
    }

    @Override
    public ResponseEntity<?> download(UUID id, HttpServletResponse response) throws IOException {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND,messageByLang.getMessageByKey("FILE_NOT_FOUND")));
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(attachment.getId());
        if (attachmentContent == null)
            throw new RestException(HttpStatus.NOT_FOUND,messageByLang.getMessageByKey("ATTACHMENT_CONTENT_NOT_FOUND"));

        response.setHeader("Content-Disposition", "attachment; filename=\"" + attachment.getName() + "\"");
        response.setContentType(attachment.getContentType());
        response.setContentLength((int) attachment.getSize());
//        FileCopyUtils.copy(attachmentContent.getBytes(),response.getOutputStream());
        return ResponseEntity.ok().header("Message",messageByLang.getMessageByKey("FILE_DOWNLOADED")).body(new InputStreamResource(new ByteArrayInputStream(attachmentContent.getBytes())));
    }

    public CustomPage<Attachment> attachmentCustomPage(Page<Attachment> attachmentPage) {
        return new CustomPage<>(
                attachmentPage.getContent(),
                attachmentPage.getNumberOfElements(),
                attachmentPage.getNumber(),
                attachmentPage.getTotalElements(),
                attachmentPage.getTotalPages(),
                attachmentPage.getSize()
        );
    }
}
