package uz.pdp.apponlineshopping.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.pdp.apponlineshopping.component.MessageByLang;

import uz.pdp.apponlineshopping.entity.Attachment;
import uz.pdp.apponlineshopping.entity.Category;
import uz.pdp.apponlineshopping.entity.Product;
import uz.pdp.apponlineshopping.entity.template.AbsEntity;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.exception.RestException;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.mapper.ProductMapper;
import uz.pdp.apponlineshopping.payload.ProductReqDto;
import uz.pdp.apponlineshopping.payload.ProductResDto;
import uz.pdp.apponlineshopping.repository.AttachmentRepository;
import uz.pdp.apponlineshopping.repository.CategoryRepository;
import uz.pdp.apponlineshopping.repository.ProductRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {

	private final ProductRepository productRepository;
	private final CategoryRepository categoryRepository;
	private final ProductMapper productMapper;
	private final MessageByLang messageByLang;
	private final AttachmentRepository attachmentRepository;

	@Override
	public ApiResult<CustomPage<ProductResDto>> getAllByNameSorted(int page, int size) {
		Pageable pageable= PageRequest.of(page, size, Sort.Direction.ASC,"name");
		Page<Product> productPage = productRepository.findAll(pageable);
		CustomPage<ProductResDto> productResDtoCustomPage=productResDtoCustomPage(productPage);
		return ApiResult.successResponse(productResDtoCustomPage);

	}

	@Override
	public ApiResult<CustomPage<ProductResDto>> getAllByPriceSorted(int page, int size) {
		Pageable pageable= PageRequest.of(page, size, Sort.Direction.ASC,"price");
		Page<Product> productPage = productRepository.findAll(pageable);
		CustomPage<ProductResDto> productResDtoCustomPage=productResDtoCustomPage(productPage);
		return ApiResult.successResponse(productResDtoCustomPage);

	}

	@Override
	public ApiResult<List<ProductResDto>> getAllByCategoryFiltered(String categoryName) {
		List<Product> productList = productRepository.findByCategoryName(categoryName);
		List<ProductResDto> productResDtoList = productList.stream().map(productMapper::productToDto).collect(Collectors.toList());
		return ApiResult.successResponse(productResDtoList);

	}

	@Override
	public ApiResult<List<ProductResDto>> getAllBySearchName(String searchName) {
		List<Product> productList=productRepository.findByNameStartingWith(searchName);
		List<ProductResDto> productResDtoList = productList.stream().map(productMapper::productToDto).collect(Collectors.toList());
		return ApiResult.successResponse(productResDtoList);
	}

	@Override
	public ApiResult<ProductResDto> getProduct(UUID id) {
		Product product = productRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND")));
		return ApiResult.successResponse(productMapper.productToDto(product));
	}

	@Override
	public ApiResult<ProductResDto> addProduct(ProductReqDto productReqDto) {

		Category category = categoryRepository.findByName(productReqDto.getCategoryName()).orElseThrow(()-> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("CATEGORY_NOT_FOUND")));

		Set<Attachment> attachments =new HashSet<>();

		if(!productReqDto.getAttachmentIds().isEmpty()){
			attachments=attachmentRepository.findAllByIdIsIn(productReqDto.getAttachmentIds());
		}

		if (productRepository.existsByName(productReqDto.getName())){
			throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("EXISTED_PRODUCT"));
		}

		Product product=new Product(
				productReqDto.getName(),
				productReqDto.getPrice(),
				productReqDto.getDescription(),
				category,
				attachments,
				null
		);

		productRepository.save(product);

		Set<UUID>attachmentsIdSet=new HashSet<>();

		if(!attachments.isEmpty()){
			attachmentsIdSet = attachments.stream().map(AbsEntity::getId).collect(Collectors.toSet());
		}

		ProductResDto productResDto = productMapper.productToDto(product);

		productResDto.setCategoryId(category.getId());

		productResDto.setAttachmentIds(attachmentsIdSet);

		return ApiResult.successResponse(productResDto);
	}

	@Override
	public ApiResult<ProductResDto> editProduct(ProductReqDto productReqDto, UUID id) {

		Product product = productRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND")));

		Category category = categoryRepository.findByName(productReqDto.getCategoryName()).orElseThrow(()-> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("CATEGORY_NOT_FOUND")));

		if (productRepository.existsByNameAndIdNot(productReqDto.getName(),id)) {
			throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("EXISTED_PRODUCT"));
		}

		Set<Attachment> attachments =new HashSet<>();

		if(!productReqDto.getAttachmentIds().isEmpty()){
			attachments=attachmentRepository.findAllByIdIsIn(productReqDto.getAttachmentIds());
		}

		product.setName(productReqDto.getName());
		product.setPrice(productReqDto.getPrice());
		product.setDescription(productReqDto.getDescription());
		product.setCategory(category);
		product.setAttachments(attachments);

		Product savedProduct = productRepository.save(product);

		Set<UUID>attachmentsIdSet=new HashSet<>();

		if(!attachments.isEmpty()){
			attachmentsIdSet = attachments.stream().map(AbsEntity::getId).collect(Collectors.toSet());
		}

		ProductResDto productResDto = productMapper.productToDto(savedProduct);

		productResDto.setCategoryId(category.getId());

		productResDto.setAttachmentIds(attachmentsIdSet);

		return ApiResult.successResponse(productResDto);

	}

	@Override
	public ApiResult<?> deleteProduct(UUID id) {

		try {
			productRepository.deleteById(id);
			return ApiResult.successResponse(messageByLang.getMessageByKey("PRODUCT_DELETED"));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND"));
		}
	}


	public CustomPage<ProductResDto> productResDtoCustomPage(Page<Product> productPage){
		return new CustomPage<>(
				productPage.getContent().stream().map(productMapper::productToDto).collect(Collectors.toList()),
				productPage.getTotalPages(),
				productPage.getNumber(),
				productPage.getTotalElements(),
				productPage.getSize(),
				productPage.getNumberOfElements()

		);
	}

}
