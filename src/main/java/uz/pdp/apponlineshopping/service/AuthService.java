package uz.pdp.apponlineshopping.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.payload.LoginDto;
import uz.pdp.apponlineshopping.payload.UserDto;
import uz.pdp.apponlineshopping.payload.TokenDto;

import java.util.UUID;

public interface AuthService extends UserDetailsService {

    UserDetails loadById(UUID id);

    ApiResult<TokenDto> signIn(LoginDto loginDto);

    ApiResult<TokenDto> signUp(UserDto userDto);

    ApiResult<TokenDto> refreshToken(TokenDto tokenDto);

    ApiResult<?> editUser(UserDto userDto, User user);

    ApiResult<?> deleteUser(User user);

}
