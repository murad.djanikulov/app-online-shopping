package uz.pdp.apponlineshopping.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.pdp.apponlineshopping.component.MessageByLang;
import uz.pdp.apponlineshopping.entity.Item;
import uz.pdp.apponlineshopping.entity.Order;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.enumeration.OrderStatus;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.exception.RestException;
import uz.pdp.apponlineshopping.mapper.AddressMapper;
import uz.pdp.apponlineshopping.payload.ItemResDto;
import uz.pdp.apponlineshopping.payload.OrderResDto;
import uz.pdp.apponlineshopping.repository.OrderRepository;

import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class OrderServiceImpl implements OrderService {

	private final OrderRepository orderRepository;
	private final AddressMapper addressMapper;
	private final MessageByLang messageByLang;

	@Override
	public ApiResult<OrderResDto> confirmAndPay(UUID id, User user){

		Order order =orderRepository.findByIdAndUser_Id(id,user.getId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND")));

		if (order.getAddress()==null) {
			throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("ADDRESS_CANNOT_BE_NULL"));
		}

		Timestamp timestamp = new Timestamp(System.currentTimeMillis() + 604800000);

		order.setDeliveryTime(timestamp);

		order.setStatus(OrderStatus.CLOSED);

		Set<ItemResDto> itemResDtos = order.getItems().stream().map(this::itemToDto).collect(Collectors.toSet());

		double orderPrice = itemResDtos.stream().map(ItemResDto::getTotalPrice).mapToDouble(i -> i).sum();

		OrderResDto orderResDto=new OrderResDto(
				order.getId(),
				order.getUser().getId(),
				order.getStatus(),
				order.getDeliveryTime(),
				addressMapper.addressToDto(order.getAddress()),
				itemResDtos,
				orderPrice
		);

		return ApiResult.successResponse(orderResDto);

	}

	@Override
	public ApiResult<OrderResDto> getOrder(UUID id, User user) {

		Order order =orderRepository.findByIdAndUser_Id(id,user.getId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND")));

		Set<ItemResDto> itemResDtos = order.getItems().stream().map(this::itemToDto).collect(Collectors.toSet());

		double orderPrice = itemResDtos.stream().map(ItemResDto::getTotalPrice).mapToDouble(i -> i).sum();

		OrderResDto orderResDto=new OrderResDto(
				order.getId(),
				order.getUser().getId(),
				order.getStatus(),
				order.getDeliveryTime()!=null?order.getDeliveryTime():null,
				order.getAddress()!=null?addressMapper.addressToDto(order.getAddress()):null,
				itemResDtos,
				orderPrice
		);
		return ApiResult.successResponse(orderResDto);
	}

	@Override
	public ApiResult<OrderResDto> addOrder(User user) {

		Order order=orderRepository.save(new Order(
				user,
				OrderStatus.PENDING
		));

		OrderResDto orderResDto=new OrderResDto(
				order.getId(),
				order.getUser().getId(),
				order.getStatus(),
				null,
				0.0
		);

		return ApiResult.successResponse(orderResDto);
	}


	@Override
	public ApiResult<?> deleteOrder(UUID id,User user) {

		int delete = orderRepository.deleteByIdAndUser_Id(id, user.getId());
		if(delete>0){
			return ApiResult.successResponse(messageByLang.getMessageByKey("ORDER_DELETED"));
		}
		throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND"));
	}

	public ItemResDto itemToDto(Item item){
		 return new ItemResDto(
				item.getId(),
				item.getProduct().getName(),
				item.getQuantity(),
				item.getTotalPrice());
	}

}
