package uz.pdp.apponlineshopping.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.pdp.apponlineshopping.component.MessageByLang;
import uz.pdp.apponlineshopping.entity.Item;
import uz.pdp.apponlineshopping.entity.Order;
import uz.pdp.apponlineshopping.entity.Product;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.exception.RestException;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.ItemReqDto;
import uz.pdp.apponlineshopping.payload.ItemResDto;
import uz.pdp.apponlineshopping.repository.ItemRepository;
import uz.pdp.apponlineshopping.repository.OrderRepository;
import uz.pdp.apponlineshopping.repository.ProductRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ItemServiceImpl implements ItemService {

	private final ItemRepository itemRepository;
	private final ProductRepository productRepository;
	private final MessageByLang messageByLang;
	private final OrderRepository orderRepository;

	@Override
	public ApiResult<List<ItemResDto>> getAllByOrderId(UUID orderId, User user) {
		List<Item> itemList = itemRepository.findByOrderIdAndOrder_User_Id(orderId, user.getId());
		List<ItemResDto> itemResDtoList = itemList.stream().map(this::itemToDto).collect(Collectors.toList());
		return ApiResult.successResponse(itemResDtoList);

	}

	@Override
	public ApiResult<ItemResDto> getItem(UUID id,User user) {
		Item item = itemRepository.findByIdAndOrder_User_Id(id,user.getId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ITEM_NOT_FOUND")));
		return ApiResult.successResponse(itemToDto(item));
	}

	@Override
	public ApiResult<ItemResDto> addItem(ItemReqDto itemReqDto,User user) {
		Order order = orderRepository.findByIdAndUser_Id(itemReqDto.getOrderId(),user.getId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND")));
		Product product = productRepository.findByName(itemReqDto.getProductName()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND")));
		Item item=new Item(product,itemReqDto.getQuantity(),(product.getPrice()*itemReqDto.getQuantity()),order);
		itemRepository.save(item);
		return ApiResult.successResponse(itemToDto(item));
	}

	@Override
	public ApiResult<ItemResDto> editItem(ItemReqDto itemReqDto, UUID id, User user) {
		Item item = itemRepository.findByIdAndOrder_User_Id(id,user.getId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ITEM_NOT_FOUND")));
		Product product = productRepository.findByName(itemReqDto.getProductName()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND")));
		item.setProduct(product);
		item.setQuantity(itemReqDto.getQuantity());
		item.setTotalPrice(product.getPrice()*itemReqDto.getQuantity());
		itemRepository.save(item);
		return ApiResult.successResponse(itemToDto(item));
	}

	@Override
	public ApiResult<?> deleteItem(UUID id, User user) {
		int delete = itemRepository.deleteByIdAndOrder_User_Id(id, user.getId());
		if(delete>0) {
			return ApiResult.successResponse(messageByLang.getMessageByKey("ITEM_DELETED"));
		}
		throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ITEM_NOT_FOUND"));
	}

	public ItemResDto itemToDto(Item item){
		ItemResDto itemResDto=new ItemResDto(
				item.getId(),
				item.getProduct().getName(),
				item.getQuantity(),
				item.getTotalPrice());
		return itemResDto;
	}

}
