package uz.pdp.apponlineshopping.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.pdp.apponlineshopping.component.MessageByLang;
import uz.pdp.apponlineshopping.entity.Address;
import uz.pdp.apponlineshopping.entity.Order;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.exception.RestException;
import uz.pdp.apponlineshopping.mapper.AddressMapper;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.AddressReqDto;
import uz.pdp.apponlineshopping.payload.AddressResDto;
import uz.pdp.apponlineshopping.repository.AddressRepository;
import uz.pdp.apponlineshopping.repository.OrderRepository;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final AddressMapper addressMapper;
    private final MessageByLang messageByLang;
    private final OrderRepository orderRepository;



    @Override
    public ApiResult<CustomPage<AddressResDto>> getAll( int page, int size) {

        Pageable pageable= PageRequest.of(page, size, Sort.Direction.ASC,"state");
        Page<Address> addressPage = addressRepository.findAll(pageable);
        CustomPage<AddressResDto> addressResDtoCustomPage=addressResDtoCustomPage(addressPage);
        return ApiResult.successResponse(addressResDtoCustomPage);

    }

    @Override
    public ApiResult<AddressResDto> getAddress(UUID id) {
        Address address = addressRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ADDRESS_NOT_FOUND")));
        return ApiResult.successResponse(addressMapper.addressToDto(address));
    }

    @Override
    public ApiResult<AddressResDto> addAddress(AddressReqDto addressDto, UUID orderId,User user) {

        Optional<Order> optionalOrder = orderRepository.findByIdAndUser_Id(orderId,user.getId());

        if(optionalOrder.isEmpty()){
            throw  new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND"));
        }
        Order order = optionalOrder.get();

        if(order.getAddress()!=null){
            throw  new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("ADDRESS_EXISTS"));
        }

        Address address = new Address(
                addressDto.getState(),
                addressDto.getCity(),
                addressDto.getStreet(),
                addressDto.getPostalCode(),
                addressDto.getHomeNumber(),
                order);

        addressRepository.save(address);
        AddressResDto addressResDto = addressMapper.addressToDto(address);
        addressResDto.setOrderId(orderId);
        return ApiResult.successResponse(addressResDto);
    }

    @Override
    public ApiResult<AddressResDto> editAddress(AddressReqDto addressDto, UUID orderId,User user) {

        Optional<Order> optionalOrder = orderRepository.findByIdAndUser_Id(orderId,user.getId());

        if(optionalOrder.isEmpty()){
            throw  new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND"));
        }

        UUID foundAddressId = optionalOrder.get().getAddress().getId();

        Address address = addressRepository.findById(foundAddressId).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ADDRESS_NOT_FOUND")));
        address.setState(addressDto.getState());
        address.setCity(addressDto.getCity());
        address.setStreet(addressDto.getStreet());
        address.setPostalCode(addressDto.getPostalCode());
        address.setHomeNumber(addressDto.getHomeNumber());

        addressRepository.save(address);
        return ApiResult.successResponse(addressMapper.addressToDto(address));
    }

    @Override
    public ApiResult<?> deleteAddress(UUID orderId,User user) {

        Optional<Order> optionalOrder = orderRepository.findByIdAndUser_Id(orderId,user.getId());

        if(optionalOrder.isEmpty()){
            throw  new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ORDER_NOT_FOUND"));
        }

        Order order = optionalOrder.get();
        UUID foundAddressId = order.getAddress().getId();
        order.setAddress(null);
        orderRepository.save(order);

        try {
            addressRepository.deleteById(foundAddressId);
            return ApiResult.successResponse(messageByLang.getMessageByKey("ADDRESS_DELETED"));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ADDRESS_NOT_FOUND"));
        }
    }
    public CustomPage<AddressResDto> addressResDtoCustomPage(Page<Address> addressPage){
        return new CustomPage<>(
                addressPage.getContent().stream().map(addressMapper::addressToDto).collect(Collectors.toList()),
                addressPage.getTotalPages(),
                addressPage.getNumber(),
                addressPage.getTotalElements(),
                addressPage.getSize(),
                addressPage.getNumberOfElements()

        );
    }

}
