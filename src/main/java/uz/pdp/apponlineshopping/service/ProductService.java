package uz.pdp.apponlineshopping.service;

import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.ProductReqDto;
import uz.pdp.apponlineshopping.payload.ProductResDto;

import java.util.List;
import java.util.UUID;

public interface ProductService {

    ApiResult<CustomPage<ProductResDto>> getAllByNameSorted(int page, int size);

    ApiResult<CustomPage<ProductResDto>> getAllByPriceSorted(int page, int size);

    ApiResult<List<ProductResDto>> getAllByCategoryFiltered(String categoryName);

    ApiResult<List<ProductResDto>> getAllBySearchName(String searchName);

    ApiResult<ProductResDto> getProduct(UUID id);

    ApiResult<ProductResDto> addProduct(ProductReqDto productReqDto);

    ApiResult<ProductResDto> editProduct(ProductReqDto productReqDto, UUID id);

    ApiResult<?> deleteProduct(UUID id);

}
