package uz.pdp.apponlineshopping.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.apponlineshopping.entity.Attachment;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public interface AttachmentService {

    ApiResult<?> upload(MultipartFile file) throws IOException;

    ApiResult<CustomPage<Attachment>> getAll(int page, int size);

    ApiResult<Attachment> getById(UUID id);

    ResponseEntity<?> download(UUID id, HttpServletResponse response) throws IOException;

}
