package uz.pdp.apponlineshopping.service;



import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.AddressReqDto;
import uz.pdp.apponlineshopping.payload.AddressResDto;

import java.util.UUID;

public interface AddressService {
    ApiResult<CustomPage<AddressResDto>> getAll(int page, int size);

    ApiResult<AddressResDto> getAddress(UUID id);

    ApiResult<AddressResDto> addAddress(AddressReqDto addressReqDto,UUID orderId,User user);

    ApiResult<AddressResDto> editAddress(AddressReqDto addressReqDto, UUID orderId,User user);

    ApiResult<?> deleteAddress(UUID orderId,User user);
}
