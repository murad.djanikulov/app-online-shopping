package uz.pdp.apponlineshopping.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.pdp.apponlineshopping.component.MessageByLang;
import uz.pdp.apponlineshopping.entity.Category;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.exception.RestException;
import uz.pdp.apponlineshopping.mapper.CategoryMapper;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.CategoryReqDto;
import uz.pdp.apponlineshopping.payload.CategoryResDto;
import uz.pdp.apponlineshopping.repository.CategoryRepository;

import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class CategoryServiceImpl implements CategoryService {

	private final CategoryRepository categoryRepository;
	private final CategoryMapper categoryMapper;
	private final MessageByLang messageByLang;

	@Override
	public ApiResult<CustomPage<CategoryResDto>> getAll(int page, int size) {
		Pageable pageable= PageRequest.of(page, size, Sort.Direction.ASC,"name");
		Page<Category> categoryPage = categoryRepository.findAll(pageable);
		CustomPage<CategoryResDto> categoryResDtoCustomPage=categoryResDtoCustomPage(categoryPage);
		return ApiResult.successResponse(categoryResDtoCustomPage);

	}

	@Override
	public ApiResult<CategoryResDto> getCategory(UUID id) {
		Category category = categoryRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("CATEGORY_NOT_FOUND")));
		return ApiResult.successResponse(categoryMapper.categoryToDto(category));
	}

	@Override
	public ApiResult<CategoryResDto> addCategory(CategoryReqDto categoryReqDto) {
		if(categoryRepository.existsByName(categoryReqDto.getName())){
			throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("EXISTED_CATEGORY"));
		}
		Category category=new Category(categoryReqDto.getName());
		categoryRepository.save(category);
		return ApiResult.successResponse(categoryMapper.categoryToDto(category));
	}

	@Override
	public ApiResult<CategoryResDto> editCategory(CategoryReqDto categoryReqDto, UUID id) {
		Category category = categoryRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("CATEGORY_NOT_FOUND")));
		if (categoryRepository.existsByNameAndIdNot(categoryReqDto.getName(),id)) {
			throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("EXISTED_CATEGORY"));
		}
		category.setName(categoryReqDto.getName());
		categoryRepository.save(category);
		return ApiResult.successResponse(categoryMapper.categoryToDto(category));
	}

	@Override
	public ApiResult<?> deleteCategory(UUID id) {
		try {
			categoryRepository.deleteById(id);
			return ApiResult.successResponse(messageByLang.getMessageByKey("CATEGORY_DELETED"));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("CATEGORY_NOT_FOUND"));
		}
	}


	public CustomPage<CategoryResDto> categoryResDtoCustomPage(Page<Category> categoryPage){
		return new CustomPage<>(
				categoryPage.getContent().stream().map(categoryMapper::categoryToDto).collect(Collectors.toList()),
				categoryPage.getTotalPages(),
				categoryPage.getNumber(),
				categoryPage.getTotalElements(),
				categoryPage.getSize(),
				categoryPage.getNumberOfElements()

		);
	}

}
