package uz.pdp.apponlineshopping.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.apponlineshopping.entity.Category;
import uz.pdp.apponlineshopping.entity.Product;
import uz.pdp.apponlineshopping.entity.Role;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.enumeration.PermissionType;
import uz.pdp.apponlineshopping.repository.CategoryRepository;
import uz.pdp.apponlineshopping.repository.ProductRepository;
import uz.pdp.apponlineshopping.repository.RoleRepository;
import uz.pdp.apponlineshopping.repository.UserRepository;
import uz.pdp.apponlineshopping.utils.AppConstant;

import java.util.Arrays;
import java.util.HashSet;

@Component

public class DataLoader implements CommandLineRunner {
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;

    public DataLoader(RoleRepository roleRepository, UserRepository userRepository, @Lazy PasswordEncoder passwordEncoder, CategoryRepository categoryRepository, ProductRepository productRepository) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
    }

    @Value("${dataLoaderMode}")
    private String dataLoaderMode;


    @Override
    public void run(String... args) throws Exception {

        if (dataLoaderMode.equals("always")) {
            Role admin = roleRepository.save(new Role(
                    AppConstant.ADMIN,
                    new HashSet<>(Arrays.asList(PermissionType.values())))
            );


            userRepository.save(new User(
                    "admin",
                    "admin",
                    "+998123456789",
                    passwordEncoder.encode("0123456789"),
                    admin
            ));

            roleRepository.save(new Role(
                    AppConstant.USER,
                    new HashSet<>())
            );

            Category mobile_phone = categoryRepository.save(new Category("Mobile phone"));
            Category washing_machine = categoryRepository.save(new Category("Washing machine"));
            Category laptop = categoryRepository.save(new Category("Laptop"));

            productRepository.saveAll(
                    Arrays.asList(
                            new Product("Apple",500.00,"Brand new",mobile_phone,new HashSet<>(),null),
                            new Product("Samsung",400.00,"Brand new",mobile_phone,new HashSet<>(),null),
                            new Product("Nokia",300.00,"Brand new",mobile_phone,new HashSet<>(),null)
                    )
            );

            productRepository.saveAll(
                    Arrays.asList(
                            new Product("LG",700.00,"Brand new",washing_machine,new HashSet<>(),null),
                            new Product("Bosch",800.00,"Brand new",washing_machine,new HashSet<>(),null),
                            new Product("Artel",600.00,"Brand new",washing_machine,new HashSet<>(),null)
                    )
            );

            productRepository.saveAll(
                    Arrays.asList(
                            new Product("Lenovo",800.00,"Brand new",laptop,new HashSet<>(),null),
                            new Product("HP",700.00,"Brand new",laptop,new HashSet<>(),null),
                            new Product("Acer",600.00,"Brand new",laptop,new HashSet<>(),null)
                    )
            );

        }
    }
}
