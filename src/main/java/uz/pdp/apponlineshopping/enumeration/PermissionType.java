package uz.pdp.apponlineshopping.enumeration;

public enum PermissionType {
    VIEW_USER("",""),
    DELETE_USER("",""),

    ADD_CATEGORY("",""),
    EDIT_CATEGORY("",""),
    DELETE_CATEGORY("",""),

    ADD_PRODUCT("",""),
    EDIT_PRODUCT("",""),
    DELETE_PRODUCT("","");


    private final String nameUz;
    private final String nameRu;

    PermissionType(String nameUz, String nameRu) {
        this.nameUz = nameUz;
        this.nameRu = nameRu;
    }

    public String getNameUz() {
        return nameUz;
    }

    public String getNameRu() {
        return nameRu;
    }
}
