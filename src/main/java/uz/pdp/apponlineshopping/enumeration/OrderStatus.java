package uz.pdp.apponlineshopping.enumeration;

public enum OrderStatus {
	
	PENDING("Kutmoqda","В ожидфании"),
	CLOSED("Yopilgan","Завершено");

	private final String nameUz;
	private final String nameRu;


	OrderStatus(String nameUz, String nameRu) {
		this.nameUz = nameUz;
		this.nameRu = nameRu;
	}

	public String getNameUz() {
		return nameUz;
	}

	public String getNameRu() {
		return nameRu;
	}
}
