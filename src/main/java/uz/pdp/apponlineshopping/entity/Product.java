package uz.pdp.apponlineshopping.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.pdp.apponlineshopping.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true,exclude = {"category","attachments","items"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "products")
@SQLDelete(sql = "update products set deleted=true where id=?")
@Where(clause = "deleted=false")

public class Product extends AbsEntity {

    @Column(nullable = false,unique = true)
	private String name;

	private Double price;

	@Column(columnDefinition = "text")
	private String description;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "category_id")
	@JsonBackReference
	private Category category;

	@OneToMany(mappedBy = "product")
	private Set<Attachment> attachments;

	@OneToMany(mappedBy="product",cascade = CascadeType.ALL)
	private List<Item> items;


}
