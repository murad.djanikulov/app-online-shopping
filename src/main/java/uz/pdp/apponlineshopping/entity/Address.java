package uz.pdp.apponlineshopping.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.pdp.apponlineshopping.entity.template.AbsEntity;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true,exclude = {"order"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update address set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Address extends AbsEntity {

    @Column(nullable = false)
    private String state;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String street;

    @Column(nullable = false)
    private String postalCode;

    @Column(nullable = false)
    private String homeNumber;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "order_id",nullable = false)
    @JsonIgnoreProperties("address")
    private Order order;
}
