package uz.pdp.apponlineshopping.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.pdp.apponlineshopping.entity.template.AbsEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true,exclude = {"attachment"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update attachment_content set deleted=true where id=?")
@Where(clause = "deleted=false")
public class AttachmentContent extends AbsEntity {

    private byte[] bytes;

    @OneToOne(optional = false,cascade = CascadeType.ALL)
    private Attachment attachment;
}
