package uz.pdp.apponlineshopping.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.pdp.apponlineshopping.entity.template.AbsEntity;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true,exclude = {"product","order"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "item")
@SQLDelete(sql = "update item set deleted=true where id=?")
@Where(clause = "deleted=false")

public class Item extends AbsEntity {

	@ManyToOne
	@JoinColumn(name = "product_id")
	@JsonIgnoreProperties("item")
	private Product product;

	private Integer quantity;

	private Double totalPrice;

	@ManyToOne
	@JoinColumn(name = "order_id", nullable = false)
	@JsonIgnoreProperties("item")
	private Order order;
}
