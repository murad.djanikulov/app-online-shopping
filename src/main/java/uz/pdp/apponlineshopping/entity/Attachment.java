package uz.pdp.apponlineshopping.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.pdp.apponlineshopping.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true,exclude = {"product"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update attachment set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Attachment extends AbsEntity {

    private String name;

    private String contentType;

    private long size;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

}
