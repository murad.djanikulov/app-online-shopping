package uz.pdp.apponlineshopping.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.pdp.apponlineshopping.entity.template.AbsEntity;
import uz.pdp.apponlineshopping.enumeration.PermissionType;

import javax.persistence.*;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update role set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Role extends AbsEntity {

    @Column(unique = true)
    private String name;

    @Enumerated(EnumType.STRING)
    @ElementCollection
    private Set<PermissionType> permissions;
}
