package uz.pdp.apponlineshopping.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import uz.pdp.apponlineshopping.entity.template.AbsEntity;
import uz.pdp.apponlineshopping.enumeration.OrderStatus;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true,exclude = {"user","address","items"})
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "orders")
@SQLDelete(sql = "update orders set deleted=true where id=?")
@Where(clause = "deleted=false")

public class Order extends AbsEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    private Timestamp deliveryTime;

    @OneToOne(mappedBy = "order",cascade = CascadeType.ALL)
    private Address address;

    @OneToMany(mappedBy = "order",cascade = CascadeType.ALL)
    private Set<Item> items;

    private Double orderPrice;

    public Order(User user, OrderStatus status) {
        this.user = user;
        this.status = status;
    }


}
