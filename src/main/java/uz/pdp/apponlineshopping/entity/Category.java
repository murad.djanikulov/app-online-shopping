package uz.pdp.apponlineshopping.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.pdp.apponlineshopping.entity.template.AbsEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Set;

@EqualsAndHashCode(callSuper = true,exclude = {"products"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "categories")
@SQLDelete(sql = "update categories set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Category extends AbsEntity {

	@Column(nullable = false, unique = true)
	private String name;

	@OneToMany(mappedBy ="category",cascade = CascadeType.ALL )
	@JsonManagedReference
	@JsonIgnore
	Set<Product> products;

	public Category(String name) {
		this.name = name;
	}
}
