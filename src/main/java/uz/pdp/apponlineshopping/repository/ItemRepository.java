package uz.pdp.apponlineshopping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import uz.pdp.apponlineshopping.entity.Item;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ItemRepository extends JpaRepository<Item, UUID> {

    List<Item> findByOrderIdAndOrder_User_Id(UUID order_id, UUID order_user_id);

    Optional<Item> findByIdAndOrder_User_Id(UUID id, UUID order_user_id);

    @Transactional
    @Modifying
    int deleteByIdAndOrder_User_Id(UUID id, UUID order_user_id);
}
