package uz.pdp.apponlineshopping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import uz.pdp.apponlineshopping.entity.Order;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {

    Optional<Order> findByIdAndUser_Id(UUID id,UUID userId);

    @Transactional
    @Modifying
    int deleteByIdAndUser_Id(UUID id, UUID userId);

//    Optional<Order> findByUser_IdAndIdAndAddress_Id(UUID user_id, UUID id, UUID address_id);

}
