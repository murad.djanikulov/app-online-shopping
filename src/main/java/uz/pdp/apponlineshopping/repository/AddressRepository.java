package uz.pdp.apponlineshopping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.apponlineshopping.entity.Address;

import java.util.UUID;

public interface AddressRepository extends JpaRepository<Address, UUID> {

}
