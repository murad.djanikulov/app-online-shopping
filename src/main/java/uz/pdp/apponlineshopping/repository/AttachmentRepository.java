package uz.pdp.apponlineshopping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.apponlineshopping.entity.Attachment;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {

    Set<Attachment> findAllByIdIsIn(Set<UUID> id);

}
