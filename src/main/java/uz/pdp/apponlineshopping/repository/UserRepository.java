package uz.pdp.apponlineshopping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.apponlineshopping.entity.User;

import java.util.Optional;
import java.util.UUID;


public interface UserRepository extends JpaRepository<User, UUID> {


    Optional<User> findByPhoneNumber(String phoneNumber);

    Optional<User> findByPhoneNumberAndIdNot(String phoneNumber, UUID userId);
}
