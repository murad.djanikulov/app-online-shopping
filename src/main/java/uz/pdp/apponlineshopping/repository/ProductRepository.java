package uz.pdp.apponlineshopping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.apponlineshopping.entity.Product;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository<Product, UUID> {


    boolean existsByName(String name);

    boolean existsByNameAndIdNot(String name, UUID id);

    Optional<Product> findByName(String name);

    List<Product> findByCategoryName(String category);

//    List<Product> findByNameContaining(String search);
    List<Product> findByNameStartingWith(String name);
}
