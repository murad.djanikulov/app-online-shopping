package uz.pdp.apponlineshopping.payload;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductResDto {

    private UUID id;

    private String name;

    private Double price;

    private String description;

    private UUID categoryId;

    private Set<UUID> attachmentIds;

}
