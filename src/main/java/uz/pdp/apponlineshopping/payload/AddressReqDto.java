package uz.pdp.apponlineshopping.payload;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class AddressReqDto {


    @NotBlank(message = "{STATE_CANNOT_BE_BLANK}")
    @Size(min=3,max = 20,message = "{STATE_LENGTH}")
    private String state;

    @NotBlank(message = "{CITY_CANNOT_BE_BLANK}")
    @Size(min=3,max = 20,message = "{CITY_LENGTH}")
    private String city;

    @NotBlank(message = "{STREET_CANNOT_BE_BLANK}")
    @Size(min=3,max = 20,message = "{STREET_LENGTH}")
    private String street;

    @NotBlank(message = "{POSTAL_CODE_CANNOT_BE_BLANK}")
    @Size(min=5,max = 10, message = "{POSTAL_CODE_LENGTH}")
    private String postalCode;

    @NotBlank(message = "{HOME_NUMBER_CANNOT_BE_BLANK}")
    @Size(min=1,max = 3,message = "{HOME_NUMBER_LENGTH}")
    private String homeNumber;


}
