package uz.pdp.apponlineshopping.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.apponlineshopping.enumeration.OrderStatus;

import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class OrderResDto {

    private UUID id;

    private UUID userId;

    private OrderStatus status;

    private Timestamp deliveryTime;

    private AddressResDto address;

    private Set<ItemResDto> items;

    private Double orderPrice;


    public OrderResDto(UUID id, UUID userId, OrderStatus status, AddressResDto address, Double orderPrice) {
        this.id = id;
        this.userId = userId;
        this.status = status;
        this.address = address;
        this.orderPrice = orderPrice;
    }

}
