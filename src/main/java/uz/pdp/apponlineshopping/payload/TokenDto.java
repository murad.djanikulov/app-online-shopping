package uz.pdp.apponlineshopping.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenDto {

    @NotBlank(message = "{REQUIRED_ACCESS}")
    private String accessToken;

    @NotBlank(message = "{REQUIRED_REFRESH}")
    private String refreshToken;


}
