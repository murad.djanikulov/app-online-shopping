package uz.pdp.apponlineshopping.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ItemReqDto {

    @Size(min = 2, max = 20)
    @NotBlank(message = "{PRODUCT_NAME_CANNOT_BE_BLANK}")
    private String productName;

    @Min(value = 1,message = "")
    @Max(value = 10,message = "")
    @Positive(message = "")
    private Integer quantity;

    private UUID orderId;

}
