package uz.pdp.apponlineshopping.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductReqDto {

    @Size(min = 3, max = 20)
    @NotBlank(message = "{PRODUCT_NAME_CANNOT_BE_BLANK}")
    private String name;

    @Positive(message = "{POSITIVE_NUMBER}")
    @Digits(integer = 5, fraction = 2,message = "{WRONG_PRICE_FORMAT}")
    private Double price;

    @Size(min = 10, max = 100, message = "{DESCRIPTION_LENGTH}")
    private String description;

    @NotBlank(message = "{CATEGORY_NAME_CANNOT_BE_BLANK}")
    private String categoryName;

    private Set<UUID> attachmentIds;

}
