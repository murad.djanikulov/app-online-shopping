package uz.pdp.apponlineshopping.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CategoryReqDto {

    @Size(min = 3, max = 20,message ="{CATEGORY_NAME_LENGTH}" )
    @NotBlank(message = "{CATEGORY_NAME_CANNOT_BE_BLANK}")
    private String name;

}
