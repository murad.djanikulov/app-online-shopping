package uz.pdp.apponlineshopping.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    @NotBlank(message = "{FIRSTNAME_CANNOT_BE_EMPTY}")
    @Size(min = 3, max = 20, message = "{FIRSTNAME_LENGTH}")
    private String firstName;

    @NotBlank(message = "{LASTNAME_CANNOT_BE_EMPTY}")
    @Size(min = 3, max = 20, message = "{LASTNAME_LENGTH}")
    private String lastName;

    @NotNull(message = "{PHONE_NUMBER_CANNOT_BE_EMPTY}")
    @Size(min = 13, max = 13, message = "{PHONE_NUMBER_LENGTH}")
    @Pattern(regexp = "[+][9][9][8][0-9]{9}", message = "{PHONE_NUMBER_PATTERN}")
    private String phoneNumber;

    @NotBlank(message = "{PASSWORD_CANNOT_BE_EMPTY}")
    @Size(min = 10, max = 20, message = "{PASSWORD_LENGTH}")
    private String password;

}
