package uz.pdp.apponlineshopping.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddressResDto {

    private UUID id;

    private String state;

    private String city;

    private String street;

    private String postalCode;

    private String homeNumber;

    private UUID orderId;


}
