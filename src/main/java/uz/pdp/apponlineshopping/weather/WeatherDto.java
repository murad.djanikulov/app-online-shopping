package uz.pdp.apponlineshopping.weather;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WeatherDto {

    @Pattern(regexp = "^[a-zA-Z]+$", message = "{ONLY_LETTERS}")
    private String location;
}
