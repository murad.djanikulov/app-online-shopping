package uz.pdp.apponlineshopping.weather;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uz.pdp.apponlineshopping.component.MessageByLang;
import uz.pdp.apponlineshopping.exception.ApiResult;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class WeatherServiceImpl implements WeatherService {

    private final MessageByLang messageByLang;

    @SneakyThrows
    @Override
    public ApiResult<WeatherData> getWeatherData(WeatherDto weatherDto) {

        WeatherData weatherData = new WeatherData();
        String apiEndPoint = "https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/";
        String location = weatherDto.getLocation();

        String param = "?unitGroup=metric&key=2ELGJCWU9ZNZUZPXUSK6PFR56";

        RestTemplate restTemplate = new RestTemplate();
        String request = apiEndPoint + location + param;
        String response = restTemplate.getForObject(request, String.class);
        weatherData = parseTimelineJson(response);


        return ApiResult.successResponse(weatherData);


    }

    private WeatherData parseTimelineJson(String rawResult) {

        WeatherData weatherData = new WeatherData();

        List<Days> daysList = new ArrayList<>();

        JSONObject timelineResponse = new JSONObject(rawResult);

        ZoneId zoneId = ZoneId.of(timelineResponse.getString("timezone"));

        String weatherDataFor = timelineResponse.getString("resolvedAddress");

        weatherData.setWeatherDataFor(weatherDataFor);

        JSONArray values = timelineResponse.getJSONArray("days");

        for (int i = 0; i < values.length(); i++) {

            JSONObject dayValue = values.getJSONObject(i);

            ZonedDateTime datetime = ZonedDateTime.ofInstant(Instant.ofEpochSecond(dayValue.getLong("datetimeEpoch")), zoneId);

            String date = datetime.format(DateTimeFormatter.ISO_LOCAL_DATE);
            double maxTemp = dayValue.getDouble("tempmax");
            double minTemp = dayValue.getDouble("tempmin");
            double humidity = dayValue.getDouble("humidity");
            double precipitation = dayValue.getDouble("precip");
            String conditions = dayValue.getString("conditions");
            double windSpeed = dayValue.getDouble("windspeed");
            LocalTime sunrise = LocalTime.parse(dayValue.getString("sunrise"));
            LocalTime sunset = LocalTime.parse(dayValue.getString("sunset"));

            Days days = new Days(date, maxTemp, minTemp, humidity, precipitation, conditions, windSpeed, sunrise, sunset);
            daysList.add(days);
        }

        weatherData.setDaysList(daysList);

        return weatherData;

    }
}