package uz.pdp.apponlineshopping.weather;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.utils.RestConstant;

import javax.validation.Valid;

@RequestMapping(RestConstant.WEATHER_CONTROLLER)
@Tag(name = "Weather operations", description = "Weather")
public interface WeatherController {

    @Operation(summary = "Getting weather data")
    @PostMapping("/info")
    ApiResult<WeatherData> getWeatherData(@RequestBody @Valid WeatherDto weatherDto);

}
