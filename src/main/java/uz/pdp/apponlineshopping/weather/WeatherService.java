package uz.pdp.apponlineshopping.weather;

import uz.pdp.apponlineshopping.exception.ApiResult;

public interface WeatherService {

    ApiResult<WeatherData> getWeatherData(WeatherDto weatherDto);

}
