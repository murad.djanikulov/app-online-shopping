package uz.pdp.apponlineshopping.weather;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.apponlineshopping.exception.ApiResult;

@RestController
@RequiredArgsConstructor
@Slf4j
public class WeatherControllerImpl implements WeatherController {

    private final WeatherService weatherService;

    @Override
    public ApiResult<WeatherData> getWeatherData(WeatherDto weatherDto) {
        log.info("WeatherController getWeatherData req weatherDto:{}", weatherDto);
        ApiResult<WeatherData> apiResult = weatherService.getWeatherData(weatherDto);
        log.info("WeatherController getWeatherData resp ApiResult:{}",apiResult);
        return apiResult;
    }



}
