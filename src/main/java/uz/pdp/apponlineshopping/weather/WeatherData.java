package uz.pdp.apponlineshopping.weather;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WeatherData {
    private String weatherDataFor;
    private List<Days> daysList;
}
