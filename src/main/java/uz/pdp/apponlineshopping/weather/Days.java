package uz.pdp.apponlineshopping.weather;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Days {
    private String date;
    private double maxTemp;
    private double minTemp;
    private double humidity;
    private double precipitation;
    private String conditions;
    private double windSpeed;
    private LocalTime sunrise;
    private LocalTime sunset;
}
