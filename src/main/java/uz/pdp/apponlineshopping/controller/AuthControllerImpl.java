package uz.pdp.apponlineshopping.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.payload.LoginDto;
import uz.pdp.apponlineshopping.payload.UserDto;
import uz.pdp.apponlineshopping.payload.TokenDto;
import uz.pdp.apponlineshopping.service.AuthService;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class AuthControllerImpl implements AuthController {

    private final AuthService authService;

    @Override
    public ApiResult<TokenDto> signIn(LoginDto loginDto) {
        return authService.signIn(loginDto);
    }

    @Override
    public ApiResult<TokenDto> signUp(UserDto userDto) {
        return authService.signUp(userDto);
    }

    @Override
    public ApiResult<TokenDto> refreshToken(TokenDto tokenDto) {
        return authService.refreshToken(tokenDto);
    }

    @Override
    public ApiResult<?> editUser(UserDto userDto, User user) {
        return authService.editUser(userDto,user);
    }

    @Override
    public ApiResult<?> deleteUser(User user) {
        return authService.deleteUser(user);
    }
}
