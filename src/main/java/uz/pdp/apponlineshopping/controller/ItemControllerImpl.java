package uz.pdp.apponlineshopping.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.ItemReqDto;
import uz.pdp.apponlineshopping.payload.ItemResDto;
import uz.pdp.apponlineshopping.service.ItemService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class ItemControllerImpl implements ItemController {

    private final ItemService itemService;

    @Override
    public ApiResult<List<ItemResDto>> getAllByOrderId(UUID orderId, User user) {
        ApiResult<List<ItemResDto>> apiResult = itemService.getAllByOrderId(orderId, user);
        return apiResult;
    }

    @Override
    public ApiResult<ItemResDto> getItem(UUID id,User user) {
        ApiResult<ItemResDto> apiResult = itemService.getItem(id,user);
        return apiResult;
    }

    @Override
    public ApiResult<ItemResDto> addItem(ItemReqDto itemReqDto, User user) {
        ApiResult<ItemResDto> apiResult = itemService.addItem(itemReqDto, user);
        return apiResult;
    }

    @Override
    public ApiResult<ItemResDto> editItem(ItemReqDto itemReqDto, UUID id, User user) {
        ApiResult<ItemResDto> apiResult = itemService.editItem(itemReqDto, id,user);
        return apiResult;
    }

    @Override
    public ApiResult<?> deleteItem(UUID id, User user) {
        ApiResult<?> apiResult = itemService.deleteItem(id,user);
        return apiResult;
    }
}
