package uz.pdp.apponlineshopping.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.payload.OrderResDto;
import uz.pdp.apponlineshopping.security.CurrentUser;
import uz.pdp.apponlineshopping.utils.RestConstant;

import java.util.UUID;

@RequestMapping(RestConstant.ORDER_CONTROLLER)
@Tag(name = "Order operations", description = "Order")
public interface OrderController {

    @Operation(summary = "Confirm and pay for an order")
    @PutMapping("/confirmAndPay/{id}")
    @Parameter(name = "user", hidden = true)
    ApiResult<OrderResDto> confirmAndPay(@PathVariable UUID id,@CurrentUser User user);


    @Operation(summary = "Getting an order")
    @GetMapping("/get/{id}")
    @Parameter(name = "user", hidden = true)
    ApiResult<OrderResDto> getOrder(@PathVariable UUID id,@CurrentUser User user);

    @Operation(summary = "Adding an order")
    @PostMapping("/add")
    @Parameter(name = "user", hidden = true)
    ApiResult<OrderResDto> addOrder(@CurrentUser User user);

    @Operation(summary = "Deleting an order")
    @DeleteMapping("/delete/{id}")
    @Parameter(name = "user", hidden = true)
    ApiResult<?> deleteOrder(@PathVariable UUID id,@CurrentUser User user);
}
