package uz.pdp.apponlineshopping.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.spi.messageinterpolation.LocaleResolver;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import uz.pdp.apponlineshopping.utils.RestConstant;

import java.util.Locale;

@RequiredArgsConstructor
@RestController
@RequestMapping(RestConstant.LOCALE_CHANGE_CONTROLLER)
@Tag(name = "Language operations(uz/en/ru)", description = "Language")
public class LocaleChangeController {

        private final ReloadableResourceBundleMessageSource messageSource;

    @Operation(summary = "Changing language")
    @PostMapping("/change")
    public String changeSessionLanguage(@RequestParam String lang) {


        String response = "ERROR";

        if ("uz".equals(lang)) {
            messageSource.setDefaultLocale(new Locale("uz"));
            response = "MUVAFFAQIYATLI";

        } else if ("en".equals(lang)) {
            messageSource.setDefaultLocale(new Locale("en"));
            response = "SUCCESSFULLY";

        } else if ("ru".equals(lang)) {
            messageSource.setDefaultLocale(new Locale("ru"));
            response = "УСПЕШНО";
        }
        return response;
    }
}
