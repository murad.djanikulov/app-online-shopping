package uz.pdp.apponlineshopping.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.ProductReqDto;
import uz.pdp.apponlineshopping.payload.ProductResDto;
import uz.pdp.apponlineshopping.utils.AppConstant;
import uz.pdp.apponlineshopping.utils.RestConstant;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstant.PRODUCT_CONTROLLER)
@Tag(name = "Product operations", description = "Product")
public interface ProductController {

    @Operation(summary = "Getting product list sorted by name")
    @GetMapping("/get-all-by-name-sorted")
    ApiResult<CustomPage<ProductResDto>> getAllByNameSorted(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @Operation(summary = "Getting product list sorted by price")
    @GetMapping("/get-all-by-price-sorted")
    ApiResult<CustomPage<ProductResDto>> getAllByPriceSorted(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                            @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @Operation(summary = "Getting product list filtered by category")
    @GetMapping("/get-all-by-category-filtered")
    ApiResult<List<ProductResDto>> getAllByCategoryFiltered(@RequestParam String categoryName);

    @Operation(summary = "Getting product list by search name")
    @GetMapping("/get-all-by-search-name")
    ApiResult<List<ProductResDto>> getAllBySearchName(@RequestParam String searchName);


    @Operation(summary = "Getting a product")
    @GetMapping("/get/{id}")
    ApiResult<ProductResDto> getProduct(@PathVariable UUID id);

    @Operation(summary = "Adding a product")
    @PostMapping("/add")
    ApiResult<ProductResDto> addProduct(@RequestBody @Valid ProductReqDto productReqDto );

    @Operation(summary = "Editing a product")
    @PutMapping("/edit/{id}")
    ApiResult<ProductResDto> editProduct(@RequestBody @Valid ProductReqDto productReqDto, @PathVariable UUID id);

    @Operation(summary = "Deleting a product")
    @DeleteMapping("/delete/{id}")
    ApiResult<?> deleteProduct(@PathVariable UUID id);
}
