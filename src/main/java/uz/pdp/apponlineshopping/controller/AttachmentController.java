package uz.pdp.apponlineshopping.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.apponlineshopping.entity.Attachment;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.utils.AppConstant;
import uz.pdp.apponlineshopping.utils.RestConstant;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RequestMapping(RestConstant.ATTACHMENT_CONTROLLER)
@Tag(name = "Attachment operations", description = "Attachment")
public interface AttachmentController {


    @Operation(summary = "Uploading attachment")
    @PostMapping(value = "/upload", consumes = {"multipart/form-data"})
    ApiResult<?> upload(@RequestPart("file") MultipartFile file) throws IOException;

    @Operation(summary = "Getting attachment list")
    @GetMapping("/info")
    ApiResult<CustomPage<Attachment>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                             @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @Operation(summary = "Getting an attachment")
    @GetMapping("/info/{id}")
    ApiResult<Attachment> getById(@PathVariable UUID id);

    @Operation(summary = "Downloading attachment")
    @GetMapping(value = "/download/{id}",produces = {MediaType.IMAGE_JPEG_VALUE,MediaType.IMAGE_GIF_VALUE,MediaType.IMAGE_PNG_VALUE})
    ResponseEntity<?> download(@PathVariable UUID id, HttpServletResponse response) throws IOException;

}
