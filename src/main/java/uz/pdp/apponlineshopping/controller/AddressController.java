package uz.pdp.apponlineshopping.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.AddressReqDto;
import uz.pdp.apponlineshopping.payload.AddressResDto;
import uz.pdp.apponlineshopping.security.CurrentUser;
import uz.pdp.apponlineshopping.utils.AppConstant;
import uz.pdp.apponlineshopping.utils.RestConstant;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(RestConstant.ADDRESS_CONTROLLER)
@Tag(name = "Address operations", description = "Address")
public interface AddressController {

    @Operation(summary = "Getting address list")
    @GetMapping("/get-all")
    ApiResult<CustomPage<AddressResDto>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @Operation(summary = "Getting an address")
    @GetMapping("/get/{id}")
    ApiResult<AddressResDto> getAddress(@PathVariable UUID id);

    @Operation(summary = "Adding an address")
    @PostMapping("/add/{orderId}")
    @Parameter(name = "user", hidden = true)
    ApiResult<AddressResDto> addAddress(@RequestBody @Valid AddressReqDto addressReqDto, @PathVariable UUID orderId, @CurrentUser User user);

    @Operation(summary = "Editing an address")
    @PutMapping("/edit/{orderId}")
    @Parameter(name = "user", hidden = true)
    ApiResult<AddressResDto> editAddress(@RequestBody @Valid AddressReqDto addressReqDto, @PathVariable UUID orderId, @CurrentUser User user);

    @Operation(summary = "Deleting an address")
    @DeleteMapping("/delete/{orderId}")
    @Parameter(name = "user", hidden = true)
    ApiResult<?> deleteAddress(@PathVariable UUID orderId, @CurrentUser User user);
}
