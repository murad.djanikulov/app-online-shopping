package uz.pdp.apponlineshopping.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.ItemReqDto;
import uz.pdp.apponlineshopping.payload.ItemResDto;
import uz.pdp.apponlineshopping.security.CurrentUser;
import uz.pdp.apponlineshopping.utils.AppConstant;
import uz.pdp.apponlineshopping.utils.RestConstant;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstant.ITEM_CONTROLLER)
@Tag(name = "Item operations", description = "Item")
public interface ItemController {

    @Operation(summary = "Getting item list by user's order")
    @GetMapping("/get-allByOrderId/{orderId}")
    @Parameter(name = "user", hidden = true)
    ApiResult<List<ItemResDto>> getAllByOrderId(@PathVariable UUID orderId, @CurrentUser User user);

    @Operation(summary = "Getting an item")
    @GetMapping("/get/{id}")
    @Parameter(name = "user", hidden = true)
    ApiResult<ItemResDto> getItem(@PathVariable UUID id, @CurrentUser User user);

    @Operation(summary = "Adding an item")
    @PostMapping("/add")
    @Parameter(name = "user", hidden = true)
    ApiResult<ItemResDto> addItem(@RequestBody @Valid ItemReqDto itemReqDto, @CurrentUser User user);

    @Operation(summary = "Editing an item")
    @PutMapping("/edit/{id}")
    @Parameter(name = "user", hidden = true)
    ApiResult<ItemResDto> editItem(@RequestBody @Valid ItemReqDto itemReqDto, @PathVariable UUID id, @CurrentUser User user);

    @Operation(summary = "Deleting an item")
    @DeleteMapping("/delete/{id}")
    @Parameter(name = "user", hidden = true)
    ApiResult<?> deleteItem(@PathVariable UUID id, @CurrentUser User user);
}
