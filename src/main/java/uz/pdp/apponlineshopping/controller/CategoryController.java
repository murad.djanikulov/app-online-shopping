package uz.pdp.apponlineshopping.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.CategoryReqDto;
import uz.pdp.apponlineshopping.payload.CategoryResDto;
import uz.pdp.apponlineshopping.utils.AppConstant;
import uz.pdp.apponlineshopping.utils.RestConstant;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(RestConstant.CATEGORY_CONTROLLER)
@Tag(name = "Category operations", description = "Category")
public interface CategoryController {

    @Operation(summary = "Getting category list")
    @GetMapping("/get-all")
    ApiResult<CustomPage<CategoryResDto>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                 @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @Operation(summary = "Getting a category")
    @GetMapping("/get/{id}")
    ApiResult<CategoryResDto> getCategory(@PathVariable UUID id);

    @Operation(summary = "Adding a category")
    @PostMapping("/add")
    ApiResult<CategoryResDto> addCategory(@RequestBody @Valid CategoryReqDto categoryReqDto);

    @Operation(summary = "Editing a category")
    @PutMapping("/edit/{id}")
    ApiResult<CategoryResDto> editCategory(@RequestBody @Valid CategoryReqDto categoryReqDto, @PathVariable UUID id);

    @Operation(summary = "Deleting a category")
    @DeleteMapping("/delete/{id}")
    ApiResult<?> deleteCategory(@PathVariable UUID id);

}
