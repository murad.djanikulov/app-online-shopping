package uz.pdp.apponlineshopping.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.payload.LoginDto;
import uz.pdp.apponlineshopping.payload.UserDto;
import uz.pdp.apponlineshopping.payload.TokenDto;
import uz.pdp.apponlineshopping.security.CurrentUser;
import uz.pdp.apponlineshopping.utils.RestConstant;

import javax.validation.Valid;

@RequestMapping(RestConstant.AUTH_CONTROLLER)
@Tag(name = "Authentication operations", description = "Authentication")
public interface AuthController {

    @Operation(summary = "Entering")
    @PostMapping("/sign-in")
    ApiResult<TokenDto> signIn(@RequestBody @Valid LoginDto loginDto);

    @Operation(summary = "Registration")
    @PostMapping("/sign-up")
    ApiResult<TokenDto> signUp(@RequestBody @Valid UserDto userDto);

    @Operation(summary = "Refresh access token")
    @PostMapping("/refresh-token")
    ApiResult<TokenDto> refreshToken(@RequestBody @Valid TokenDto tokenDto);

    @Operation(summary = "Editing the user details")
    @PostMapping("/edit-user")
    @Parameter(name = "user", hidden = true)
    ApiResult<?> editUser(@RequestBody @Valid UserDto userDto,@CurrentUser User user);

    @Operation(summary = "Deleting the user")
    @PostMapping("/delete-user")
    @Parameter(name = "user", hidden = true)
    ApiResult<?> deleteUser(@CurrentUser User user);


}
