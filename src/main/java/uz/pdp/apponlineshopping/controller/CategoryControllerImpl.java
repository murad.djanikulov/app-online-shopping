package uz.pdp.apponlineshopping.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.CategoryReqDto;
import uz.pdp.apponlineshopping.payload.CategoryResDto;
import uz.pdp.apponlineshopping.service.CategoryService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CategoryControllerImpl implements CategoryController {

    private final CategoryService categoryService;

    @Override
    public ApiResult<CustomPage<CategoryResDto>> getAll(int page, int size) {
        log.info("CategoryController getAll req page:{}, size:{}", page, size);
        ApiResult<CustomPage<CategoryResDto>> apiResult = categoryService.getAll(page, size);
        log.info("CategoryController getAll resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<CategoryResDto> getCategory(UUID id) {
        log.info("CategoryController getCategory req id:{}", id);
        ApiResult<CategoryResDto> apiResult = categoryService.getCategory(id);
        log.info("CategoryController getCategory resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @PreAuthorize(value = "hasAuthority('ADD_CATEGORY')")
    @Override
    public ApiResult<CategoryResDto> addCategory(CategoryReqDto categoryReqDto) {
        log.info("CategoryController addCategory req categoryReqDto:{}", categoryReqDto);
        ApiResult<CategoryResDto> apiResult = categoryService.addCategory(categoryReqDto);
        log.info("CategoryController addCategory resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @PreAuthorize(value = "hasAuthority('EDIT_CATEGORY')")
    @Override
    public ApiResult<CategoryResDto> editCategory(CategoryReqDto categoryReqDto, UUID id) {
        log.info("CategoryController editCategory req categoryReqDto:{}, id:{}", categoryReqDto,id);
        ApiResult<CategoryResDto> apiResult = categoryService.editCategory(categoryReqDto, id);
        log.info("CategoryController editCategory resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @PreAuthorize(value = "hasAuthority('DELETE_CATEGORY')")
    @Override
    public ApiResult<?> deleteCategory(UUID id) {
        log.info("CategoryController deleteCategory req id:{}", id);
        ApiResult<?> apiResult = categoryService.deleteCategory(id);
        log.info("CategoryController deleteCategory resp ApiResult:{}",apiResult);
        return apiResult;
    }

}
