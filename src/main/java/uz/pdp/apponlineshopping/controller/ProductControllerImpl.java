package uz.pdp.apponlineshopping.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.ProductReqDto;
import uz.pdp.apponlineshopping.payload.ProductResDto;
import uz.pdp.apponlineshopping.service.ProductService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Slf4j
public class ProductControllerImpl implements ProductController {

    private final ProductService productService;

    @Override
    public ApiResult<CustomPage<ProductResDto>> getAllByNameSorted(int page, int size) {
        log.info("ProductController getAll req page:{}, size:{}", page, size);
        ApiResult<CustomPage<ProductResDto>> apiResult = productService.getAllByNameSorted(page, size);
        log.info("ProductController getAll resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<CustomPage<ProductResDto>> getAllByPriceSorted(int page, int size) {
        log.info("ProductController getAll req page:{}, size:{}", page, size);
        ApiResult<CustomPage<ProductResDto>> apiResult = productService.getAllByPriceSorted(page, size);
        log.info("ProductController getAll resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<List<ProductResDto>> getAllByCategoryFiltered(String categoryName) {
        log.info("ProductController getAll req name:{}",categoryName);
        ApiResult<List<ProductResDto>> apiResult = productService.getAllByCategoryFiltered(categoryName);
        log.info("ProductController getAll resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<List<ProductResDto>> getAllBySearchName(String searchName) {
        log.info("ProductController getAll req searchName:{}", searchName);
        ApiResult<List<ProductResDto>> apiResult = productService.getAllBySearchName(searchName);
        log.info("ProductController getAll resp ApiResult:{}",apiResult);
        return apiResult;
    }


    @Override
    public ApiResult<ProductResDto> getProduct(UUID id) {
        log.info("ProductController getProduct req id:{}", id);
        ApiResult<ProductResDto> apiResult = productService.getProduct(id);
        log.info("ProductController getProduct resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @PreAuthorize(value = "hasAuthority('ADD_PRODUCT')")
    @Override
    public ApiResult<ProductResDto> addProduct(ProductReqDto productReqDto) {
        log.info("ProductController addProduct req productReqDto:{}",productReqDto);
        ApiResult<ProductResDto> apiResult = productService.addProduct(productReqDto);
        log.info("ProductController addProduct resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @PreAuthorize(value = "hasAuthority('EDIT_PRODUCT')")
    @Override
    public ApiResult<ProductResDto> editProduct(ProductReqDto productReqDto, UUID id) {
        log.info("ProductController editProduct req productReqDto:{}, id:{}", productReqDto,id);
        ApiResult<ProductResDto> apiResult = productService.editProduct(productReqDto, id);
        log.info("ProductController editProduct resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @PreAuthorize(value = "hasAuthority('DELETE_PRODUCT')")
    @Override
    public ApiResult<?> deleteProduct(UUID id) {
        log.info("ProductController deleteProduct req id:{}", id);
        ApiResult<?> apiResult = productService.deleteProduct(id);
        log.info("ProductController deleteProduct resp ApiResult:{}",apiResult);
        return apiResult;
    }
}
