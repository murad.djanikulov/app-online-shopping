package uz.pdp.apponlineshopping.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.payload.OrderResDto;
import uz.pdp.apponlineshopping.service.OrderService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Slf4j
public class OrderControllerImpl implements OrderController {

    private final OrderService orderService;

    @Override
    public ApiResult<OrderResDto> confirmAndPay(UUID id,User user) {
        log.info("OrderController getOrder req id:{}", id);
        ApiResult<OrderResDto> apiResult = orderService.confirmAndPay(id,user);
        log.info("AddressController getAddress resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<OrderResDto> getOrder(UUID id,User user) {
        log.info("OrderController getOrder req id:{}", id);
        ApiResult<OrderResDto> apiResult = orderService.getOrder(id,user);
        log.info("AddressController getAddress resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<OrderResDto> addOrder(User user) {
        log.info("OrderController addOrder req user:{}",user);
        ApiResult<OrderResDto> apiResult = orderService.addOrder(user);
        log.info("OrderController addOrder resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> deleteOrder(UUID id, User user) {
        log.info("OrderController deleteOrder req id:{}", id);
        ApiResult<?> apiResult = orderService.deleteOrder(id,user);
        log.info("OrderController deleteOrder resp ApiResult:{}",apiResult);
        return apiResult;
    }
}
