package uz.pdp.apponlineshopping.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.apponlineshopping.entity.User;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.payload.AddressReqDto;
import uz.pdp.apponlineshopping.payload.AddressResDto;
import uz.pdp.apponlineshopping.service.AddressService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Slf4j
public class AddressControllerImpl implements AddressController {

    private final AddressService addressService;

    @Override
    public ApiResult<CustomPage<AddressResDto>> getAll(int page, int size) {
        log.info("AddressController getAll req page:{}, size:{}", page, size);
        ApiResult<CustomPage<AddressResDto>> apiResult = addressService.getAll(page, size);
        log.info("AddressController getAll resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<AddressResDto> getAddress(UUID id) {
        log.info("AddressController getAddress req id:{}", id);
        ApiResult<AddressResDto> apiResult = addressService.getAddress(id);
        log.info("AddressController getAddress resp ApiResult:{}",apiResult);
        return apiResult;
    }


    @Override
    public ApiResult<AddressResDto> addAddress(AddressReqDto addressReqDto, UUID orderId,User user) {
        log.info("AddressController addAddress req addressReqDto:{},orderId:{}", addressReqDto,orderId);
        ApiResult<AddressResDto> apiResult = addressService.addAddress(addressReqDto,orderId,user);
        log.info("AddressController addAddress resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<AddressResDto> editAddress(AddressReqDto addressReqDto, UUID orderId,User user) {
        log.info("AddressController editAddress req addressReqDto:{}, orderId:{}", addressReqDto,orderId);
        ApiResult<AddressResDto> apiResult = addressService.editAddress(addressReqDto, orderId,user);
        log.info("AddressController editAddress resp ApiResult:{}",apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> deleteAddress(UUID orderId,User user) {
        log.info("AddressController deleteAddress req orderId:{}", orderId);
        ApiResult<?> apiResult = addressService.deleteAddress(orderId,user);
        log.info("AddressController deleteAddress resp ApiResult:{}",apiResult);
        return apiResult;
    }
}
