package uz.pdp.apponlineshopping.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.apponlineshopping.entity.Attachment;
import uz.pdp.apponlineshopping.exception.ApiResult;
import uz.pdp.apponlineshopping.mapper.CustomPage;
import uz.pdp.apponlineshopping.service.AttachmentService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class AttachmentControllerImpl implements AttachmentController {

    private final AttachmentService attachmentService;


    @Override
    public ApiResult<?> upload(MultipartFile file) throws IOException {
        return attachmentService.upload(file);
    }

    @Override
    public ApiResult<CustomPage<Attachment>> getAll(int page, int size) {
        return attachmentService.getAll(page, size);
    }


    @Override
    public ApiResult<Attachment> getById(UUID id) {
        return attachmentService.getById(id);
    }

    @Override
    public ResponseEntity<?> download(UUID id, HttpServletResponse response) throws IOException {
        return attachmentService.download(id, response);

    }
}
