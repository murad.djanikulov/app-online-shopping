package uz.pdp.apponlineshopping.exception;


import io.jsonwebtoken.ExpiredJwtException;
import lombok.RequiredArgsConstructor;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import uz.pdp.apponlineshopping.component.MessageByLang;

import java.nio.file.AccessDeniedException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@RequiredArgsConstructor
@RestControllerAdvice
public class RestExceptionHandler {

    private final MessageByLang messageByLang;

    @ExceptionHandler(value = RestException.class)
    public ResponseEntity<?> exceptionHandling(RestException restException) {
        restException.printStackTrace();
        if (restException.getErrorData()!=null&&!restException.getErrorData().isEmpty())
            return ResponseEntity.status(restException.getStatus())
                    .body(ApiResult.errorResponse(restException.getErrorData()));

        return ResponseEntity.
                status(restException.getStatus())
                .body(ApiResult.errorResponse(restException.getMessage(),restException.getObject()));
    }

    @ExceptionHandler(value = ExpiredJwtException.class)
    public ResponseEntity<?> exceptionHandling(ExpiredJwtException e) {
        return ResponseEntity.
                status(498)
                .body(ApiResult.errorResponse("Token expired"));
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<?> exceptionHandling(MethodArgumentNotValidException e){
        List<ErrorData> errorData = new ArrayList<>();
        e.getBindingResult().getAllErrors().forEach(objectError -> errorData.add(
                new ErrorData(objectError.getDefaultMessage(), 400)
                )
        );
        return ResponseEntity.status(400).body(ApiResult.errorResponse(errorData));
    }


    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<?> exceptionHandling(Exception e){
        e.printStackTrace();
        return ResponseEntity
                .status(500)
                .body(ApiResult.errorResponse("Internal server error"));
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseEntity<?> exceptionHandling(HttpMessageNotReadableException ex) {
        ex.printStackTrace();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ApiResult.errorResponse("Incorrect parameter type!"));
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public ResponseEntity<?> exceptionHandling(AccessDeniedException ex) {
        ex.printStackTrace();
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(ApiResult.errorResponse("Not allowed to enter"));
    }

    @ExceptionHandler(value = HttpClientErrorException.class)
    public ResponseEntity<?> exceptionHandling(HttpClientErrorException e) {
        e.printStackTrace();
        return ResponseEntity.
                status(400)
                .body(ApiResult.errorResponse("Bad request"));
    }




    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    public ResponseEntity<?> handleException(DataIntegrityViolationException ex) {

        ex.printStackTrace();

        try {
            SQLException sqlException = ((ConstraintViolationException) ex.getCause()).getSQLException();

            String message = sqlException.getMessage();

            String detail = "Detail:";

            int target = message.indexOf(detail);

            int fromColumnName = message.indexOf("(", target) + 1;

            int toColumnName = message.indexOf(")", fromColumnName) - 3;

            String columnName = message.substring(fromColumnName, toColumnName).toUpperCase(Locale.ROOT);

            String clientMessage = columnName + "_NOT_FOUND";

            return new ResponseEntity<>(
                    ApiResult.errorResponse(messageByLang.getMessageByKey(clientMessage)),
                    HttpStatus.NOT_FOUND
            );

        } catch (Exception exception) {

            exception.printStackTrace();

            return new ResponseEntity<>(
                    ApiResult.errorResponse("Server error. Please try again"),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
