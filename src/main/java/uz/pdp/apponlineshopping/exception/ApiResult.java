package uz.pdp.apponlineshopping.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)  //frontend null hiding
public class ApiResult<T> {

    private String message;
    private T data;
    private List<ErrorData> errors;
    private boolean success;


    public static <T> ApiResult<T> successResponse(T data) {
        return new ApiResult<>(null, data, null, true);
    }

    public static ApiResult<?> successResponse(String message) {
        return new ApiResult<>(message, null, null, true);
    }

    public static ApiResult<?> errorResponse(String message) { return new ApiResult<>(message, null, null, false); }

    public static <T>ApiResult<T> errorResponse(String message, T data) { return new ApiResult<T>(message, data, null, false); }

    public static ApiResult<?> errorResponse(List<ErrorData> errors) { return new ApiResult<>(null, null, errors, false); }

}
