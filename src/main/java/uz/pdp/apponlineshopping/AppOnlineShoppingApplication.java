package uz.pdp.apponlineshopping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppOnlineShoppingApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppOnlineShoppingApplication.class, args);
    }

}
